# Addon script for TortoiseGit. 
# The script publishes a review request from the current branch in review board platform.
# This script must be used in a post-commit or post-push hook.
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox, simpledialog
import subprocess as cmd
import sys
import os

class CredentialUI(ttk.Frame):
  
  __review_board_api = ""
  
  def __init__(self, master):
    ttk.Frame.__init__(self, master)
    self.master.title("Review Board credentials")
    self.__createWidgets()
    # Center windows on screen
    self.master.update_idletasks()
    x = (self.master.winfo_screenwidth() - self.master.winfo_width()) / 2
    y = (self.master.winfo_screenheight() - self.master.winfo_height()) / 2
    self.master.geometry("+%d+%d" % (x, y))
    self.master.wait_window(self)
 
  def __createWidgets(self):
  
    self.CredentialLabel = ttk.Label(self, text="Enter your Review Board API token:")
    self.CredentialLabel.grid(row=0, column=0, pady=5, padx=15, sticky=tk.E+tk.W)
    self.CredentialEntry = ttk.Entry(self);
    self.CredentialEntry.grid(row=1, column=0, padx=15, sticky=tk.E+tk.W)
    self.OkButton = ttk.Button(self, text="OK", command = self.__onClickOk)
    self.OkButton.grid(row=2, column=0, pady=5)
    
    self.grid()

  def __onClickOk(self):
    self.__review_board_api = self.CredentialEntry.get()
    self.destroy()    
    
  def GetCredentials(self):
    return self.__review_board_api;
    
class InfoUI(ttk.Frame):
  def __init__(self, master, credential):
    ttk.Frame.__init__(self, master)
    self.master.title("Review Board for TortoiseGit")
    self.credential = credential
    self.__createWidgets()
    # Center windows on screen
    self.master.update_idletasks()
    x = (self.master.winfo_screenwidth() - self.master.winfo_width()) / 2
    y = (self.master.winfo_screenheight() - self.master.winfo_height()) / 2
    self.master.geometry("+%d+%d" % (x, y))
    self.master.after(100, self.__executeCommand)
    
  def __createWidgets(self):
    self.InfoText = tk.Text(self, height=10, width=50, wrap=tk.WORD)
    self.ScrollText = ttk.Scrollbar(self, command=self.InfoText.yview)
    self.ScrollText.grid(row=0, column=1, sticky=tk.N+tk.S)
    self.InfoText.config(yscrollcommand=self.ScrollText.set)
    self.InfoText.grid(row=0, column=0)
    
    self.OkButton = ttk.Button(self, text="OK", command = self.__onClickOk)
    self.OkButton.grid(row=1, columnspan=2, sticky=tk.E, pady=5, padx=15)

    self.InfoText.insert(tk.END, "Executing rbt post...\n")
    self.grid()
    
  def __onClickOk(self):
    self.master.destroy()
    
  def __executeCommand(self):
    try:
      startupinfo = cmd.STARTUPINFO()
      startupinfo.dwFlags |= cmd.STARTF_USESHOWWINDOW
      output = cmd.run("rbt post --api-token " + self.credential,
                       capture_output=True, text=True, check=True, startupinfo=startupinfo)
      self.InfoText.insert(tk.END, output.stdout + "\nDone.\n")
    except Exception as error:
      self.InfoText.tag_configure("color", foreground='#FF0000')
      if error == cmd.CalledProcessError:
        self.InfoText.insert(tk.END, "Error:\n" + error.stderr + "\n", "color")
      else:
        self.InfoText.insert(tk.END, "Error:\nCould not execute rbt post.\n", "color")
  
if __name__ == "__main__":
  root = tk.Tk()
  root.withdraw()
  root.resizable(False, False)
  # Center windows on screen
  root.update_idletasks()
  x = (root.winfo_screenwidth() - root.winfo_reqwidth()) / 2
  y = (root.winfo_screenheight() - root.winfo_reqheight()) / 2
  root.geometry("+%d+%d" % (x, y))
    
  answer = messagebox.askyesno("Review Board", "Do you want to submit a code review?", parent=root)
  if not answer:
    sys.exit(0)
  
  credential = ""
  credential_file = os.path.dirname(__file__) + "\\reviewboard.dat"
  try:
    with open(credential_file, "r") as file:
      credential = file.readline()
  except:
    try:
      root.deiconify()
      credentialDialog = CredentialUI(root)
      root.iconify()
      credential = credentialDialog.GetCredentials()
      if not credential:
        sys.exit(0)
      with open(credential_file, "w") as file:
        file.write(credential + "\n")
    except:
      messagebox.showerror("Error", "Could not save the credentials.", parent=root)
      sys.exit(1) 

  root.deiconify()
  app = InfoUI(root, credential.strip())
  app.mainloop()
  