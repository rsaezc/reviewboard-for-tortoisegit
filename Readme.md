# Review Board addon for TortoiseGit
This TortoiseGit addon publishes review requests to Review Board on demand.

## Dependencies
- Python v3.7 or newer (it should include package manager "pip"): https://www.python.org/downloads/
		Add Python root folder and Python scripts folder to environment variable "Path":
      - Example for Python v3.7 installed in "C:\": add ";C:\Python37;C:\Python37\Scripts" at the end of path variable.

- RBTools v1.0.1 or newer:
    - Install from python: pip install -U RBTools
      
## Usage
- Create an API Token from your Review Board account (<your_server>/account/preferences/#api-tokens)
- Prepare your repository to use Review Board:
  - Execute "rbt setup-repo"
- Configure your TortoiseGit hooks to use the addon (TortoiseGit -> Settings -> Hook Scripts -> Add...):
  - Post-Commit hook and Post-Push hook are the best choices.
  - Type * in "Run when working tree path is under:"
  - Type "pythonw <path_to_reviewboard.py>" in "Command Line to Execute:"
  - Check  "Wait for the script to finish"
- After the first execution, introduce your API Token in the credential pop-up window. The token will be stored to prevent recurrent credential prompts.